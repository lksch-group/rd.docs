{% for item in items %}

=== "{{ item.icon or '' }}{{ item.name }}"
    {% if item.logo_url %}
    ![{{ item.name }}]({{ item.logo_url }}){ align=right width={{ item.logo_width or 65 }} }  
    {% endif %}

    {% for item in item.pros %}
    :octicons-check-16: {{ item }}
    {% endfor %}
    {% for item in item.cons %}
    :octicons-x-16: {{ item }}
    {% endfor %}

    {% if item.power %}
    :material-power-plug: {{ item.power + ' W' }}  
    {% endif %}

    {% if item.CHF %}
    :moneybag: {{ 'CHF ' + item.CHF }}  
    {% endif %}

{% endfor %}
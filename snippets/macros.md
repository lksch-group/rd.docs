{%- macro file_include(path, lang=None, file_hint=False) %}
{%- set ending = path.split('.') | last -%}
{%- set mapping = {'py': 'python', 'sh': 'bash', 'yml': 'yaml'} -%}
{% set icons = {
  'py': ':material-language-python:',
  'js': ':material-language-javascript:'
} %}
{%- set language = lang or mapping[ending] | default('bash') -%}
{%- if file_hint %}
{{ icons[file_ending] | default(':octicons-file-24:') }} **{{ path }}**
{%- endif %}
``` {{ language }}{% if not file_hint %} title="{{ path }}"{% endif %}
--8<-- "{{ path }}"
```
{%- endmacro %}

{%- macro pytest_include(file_path, root='examples/python/') -%}
{% set fp = root + file_path %}
``` python title="{{ file_path }}"
--8<-- "{{ root + file_path }}"
```
*(This script is complete, it should run "as is")*
{%- endmacro -%}

{%- macro keyboard_shortcut(keys) -%}
++{% for key in keys %}{{ key }}+{% endfor %}+
{%- endmacro -%}


{%- macro email_link(email, display_name=None) -%}
<a href="mailto:{{ email }}">{{ display_name or email }}</a>
{%- endmacro -%}

{%- macro md_link(url, label=None) -%}
[{{ label or url }}]({{ url }})
{%- endmacro -%}

{%- macro md_image(url, alt=None, classes=None) -%}
![{{ alt or 'image' }}]({{ url }}) {% if classes %}{ {{ classes | join(' ') }} }{% endif %}
{%- endmacro -%}

{%- macro image(src, alt=None, classes=None, size=None, width=None, height=None) -%}
<img src="{{ src }}" alt="{{ alt or 'image' }}"{% if size %} size="{{ size }}"{% endif %}{% if classes %} class="{{ classes | join(' ') }}"{% endif %}{% if width %} width="{{ width }}"{% endif %}{% if height %} height="{{ height }}"{%- endif %}>
{%- endmacro -%}

{%- macro format_date(date_, fmt=None) -%}
{%- if date_ is string -%}
{{ date_ }}
{%- else -%}
{{ date_.strftime(fmt or date_default_fmt) }}
{%- endif -%}
{%- endmacro -%}

{%- macro linkify(text) -%}
{% set t = text | string %}
{%- if t.startswith('https://') or t.startswith('http://') -%}
[{{ t }}]({{ t }})
{%- else -%}
{{ t }}
{%- endif -%}
{%- endmacro -%}

{%- macro table_header(headers) %}
| {{ headers | join (' | ')}} |  
| {% for i in headers %} - |{% endfor -%}
{%- endmacro -%}

{%- macro table_row(row_data, row_macro=None) -%}
| {%- for item in row_data %} {{ linkify(item) }} | {% endfor %}
{%- endmacro %}

{%- macro table(headers, data, row_macro=None) %}
| {{ headers | join (' | ')}} |  
| {% for i in headers %} - |{% endfor %}  
{% for line in data -%}
| {%- for row in line %} {{ linkify(row) }} | {% endfor %}
{% endfor %}
{%- endmacro %}

{%- macro mailto_icon(email) -%}
{{ email_link(email, ':octicons-mail-24:') }}
{%- endmacro -%}

{%- macro homepage_icon(url) -%}
{%- if url -%}
{{ md_link(url, ':octicons-home-24:') }}
{%- endif -%}
{%- endmacro -%}

{%- macro gh_avatar_image(url, name, size=50, as_link=True) -%}
{% set html_url = 'https://github.com/' + name %}
{%- if not as_link -%}
{{ image(url, name, classes=['avatar', 'circle'], size=size, width=size, height=size) }}
{%- else -%}
{{ 
md_link(
html_url, 
image(url, name, classes=['avatar', 'circle'], size=size, width=size, height=size)
)
}}
{%- endif -%}
{%- endmacro -%}

{%- macro check_mark(value, name=None) -%}
{%- if value %}:white_check_mark:{% else %}:x:{% endif %}{% if name %} {{ name }}{% endif -%}
{%- endmacro -%}

{%- macro pros_cons(
pros, cons, pro_label="Pros", con_label="Cons", 
pro_icon=':octicons-check-16:',
con_icon=':octicons-x-16:',
tabbed=False
) -%}
{% if tabbed %}
=== "{{ pro_label }}"

    {% for item in pros %}
    {{ pro_icon }} {{ item }}
    {% endfor %}

=== "{{ con_label }}"

    {% for item in cons %}
    {{ con_icon }} {{ item }}
    {% endfor %}
{%- else -%}
**{{ pro_label }}**

{% for item in pros %}
{{ pro_icon }} {{ item }}
{% endfor %}

**{{ con_label }}**

{% for item in cons %}
{{ con_icon }} {{ item }}
{% endfor %}

{% endif %}
{%- endmacro %}

{% macro html_input(id, label, type='text', placeholder='', stretch=False) %}
<input id="{{ id }}" type="{{ type }}" name="{{ id }}" placeholder="{{ placeholder }}" class="md-input input{% if stretch %} md-input--stretch{% endif %}">
{% endmacro %}

{%- macro html_props(props) -%}
{% for k,v in props.items() %} {{ k }}="{{ v }}"{% endfor %}
{%- endmacro -%}

{% macro textarea(id, content=None, placeholder='', rows=10, cols=40, props={}) %}
<textarea id="{{ id }}" class="md-input input " placeholder="{{ placeholder }}" rows="{{ rows }}" cols="{{ cols }}"{{ html_props(props) }}>{{ content or '' }}</textarea>
{% endmacro %}

{%- macro pip_install_tabs(packages=[]) -%}
=== "poetry"

    ```
    poetry add {% for item in packages %}{{ item }} {% endfor %}
    ```

=== "pip"

    ```
    pip install {% for item in packages %}{{ item }} {% endfor %}
    ```
{%- endmacro -%}
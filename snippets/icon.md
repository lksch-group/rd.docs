---
platforms:
  Linux: ''
  RaspberryPiOS: ''
devices:
  Raspi: ''
  Laptop: ':fontawesome-solid-laptop:'
misc:
  power: ':material-power-plug:'
icons: platforms + devices + misc
---
{% if name is undefined %}
{% item.name in icons %}
icons[item.name]
{% endif %}
{% elif name in icons %}
icons[name]
{% endif %}
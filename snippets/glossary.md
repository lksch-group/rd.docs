*[OS]: Operating System (Betriebssystem)
*[PC]: Personal Computer
*[GPL]: GNU General Public Licence
*[Router]: Netzwerkrouter
*[Distro]: Linux-Distribution ist eine Auswahl aufeinander abgestimmter Software um den Linux-Kernel
*[Distros]: Linux-Distribution ist eine Auswahl aufeinander abgestimmter Software um den Linux-Kernel
*[OSX]: Mac OS X
*[Kernel]: Systemkern, der unterhalb des Betriebssystems den Zugriff auf die Hardware regelt
*[apt]: Advanced Package Tool (Debian)
*[rpm]: RedHat Package Manager
*[Raspi]: Raspberry Pi Mikrocomputer
*[4K]: Auflösung von 3840x2160 Pixel
*[1080p]: Auflösung von 1920x1080 Pixel, p steht für progressiv
*[sudo]: steht für super user do
*[CCC]: Chaos Computer Club
*[PWA]: Progressive Web App. Eine neuartige Form der Webapp, die auch ohne Internetverbindung gut bedienbar ist.
*[V8]: Google open-source V8 Javascript und Webassembly engine geschrieben in C++
*[Edge Browser]: Microsoft's Edge Browser, der Nachfolger des Internet Explorer
*[Chromium]: OpenSource Browser-Kern hinter Chrome, Edge Browser
*[Chromium OS]: OpenSource Kern hinter Chrome OS
*[Sandbox]: Ein Sicherheitsmechanismus, um Programme bei Ausführung zu isolieren
*[Bug]: Ein Fehler im Programm
*[Bugs]: Fehler in einem Programm
*[PulseAudio]: Vielverwendeter SoundServer auf Linux und MacOS
*[RaaS]: Ransomware as a service
*[Randsomeware]: Erpressungsprogramm, welches Daten verschlüsselt (und exfiltriert)
*[Repository]: Eine Quelle für Programme, die als Pakete versioniert installiert werden können
*[Repositories]: Paket-Quellen für Programme
*[AWS]: Amazon Web Services, grösster Cloud Provider der Welt vor Google und MS
*[Alibaba]: Einer der grössten Chinesischen Tech-Konzernen
*[EU]: Europäische Union / European Union
*[DSGVO]: Datenschutz-Grundverordnung, seit 25.03.2018
*[GDPR]: General Data Protection Regulation, in Kraft seit 25.03.2018
*[LTS]: Long Term Support (Langzeitsupport)
*[DLSS]: Deep Learning Super Sampling Technologie von Nvidia
*[TPM]: Trusted Platform Module
*[CSAM]: Child Sexual Abuse Material
*[EFF]: Electronic Frontier Foundation
*[AI]: Artifizielle Intelligenz
*[GAFAM]: Akronym für die Big-Five Google, Amazon, Facebook, Apple, Microsoft
*[DNS]: Domain Name System
*[live]: in Echtzeit
*[LAN]: Local Area Network
*[WLAN]: Wireless Local Area Network
*[DHCP]: Dynamic Host Configuration Protocol
*[IDE]: Integrated Development Environment
*[cli]: Command line interface
*[CICD]: Continuous delivery, continuous deployment
*[Jinja2]: Python templating engine
*[FOSS]: Free & Open Source Software
*[GUI]: Graphic User Interface
*[SBC]: Single board computer, z.B. Raspberry Pi
*[DSA]: Digital Services Act der EU
*[IaC]: Infrastructure as Code
*[WAN]: Wide Area Network
*[AP]: Access Point (WIFI)
*[NAS]: Network Attached Storage
*[ufw]: umcomplicated firewall (Gnu/Linux)
*[DSL]: domain specific language (GraphQL, SQL, HCL)
*[LAMP]: Linux, Apache, MySQL, PHP
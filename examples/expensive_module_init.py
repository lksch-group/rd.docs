# calculate.py

from time import sleep


def load_data(count: int = 3):
    sleep(count)
    return []


data = load_data(2)


def transform():
    return (entry["id"] for entry in data)

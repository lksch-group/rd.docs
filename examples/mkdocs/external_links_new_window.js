function is_external_link(link) {
    if (link.href.startsWith('#')) {
        return false
    }
    if (link.href.includes('javascript:')) {
        return false
    }
    if (link.href.includes('mailto:')) {
        return false
    }
    if (link.href.includes('tel:')) {
        return false
    }
    return link.hostname !== window.location.hostname;
}

function init_external_links() {
    var links = document.links;
    for (var i = 0, linksLength = links.length; i < linksLength; i++) {
      if (is_external_link(links[i])) {
          links[i].target = '_blank';
      }
    }
}

init_external_links()

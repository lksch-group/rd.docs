from pathlib import Path

from pydantic import SecretStr, AnyUrl, BaseSettings, Field

FILE_PATH = Path(__file__)
ENV_FILE = FILE_PATH.parent / "env"


class CustomBaseSettings(BaseSettings):
    @classmethod
    def from_env(cls, env_file: Path):
        """Allows for sensible startup for the application using the environment"""
        return cls(_env_file=env_file if env_file.is_file() else None)


class DBConfigCS(CustomBaseSettings):
    user: str
    password: SecretStr

    class Config:
        # prevents reading USER env
        case_sensitive = True


class APIConfig(CustomBaseSettings):
    """Implementation checking for env file existence, else reading from env vars only."""

    host: AnyUrl
    port: int
    db: DBConfigCS = Field(default_factory=lambda: DBConfigCS.from_env(ENV_FILE))


cfg = APIConfig(_env_file=ENV_FILE)
assert cfg.port == 1111
assert cfg.db.user == "admin"
print(cfg.json(indent=2))
"""
{
  "host": "ftp://foo.bar",
  "port": 1111,
  "db": {
    "user": "admin",
    "password": "**********"
  }
}
"""


# else this is also possible, getting the same validation
cfg_2 = APIConfig(_env_file=ENV_FILE, db=DBConfigCS(_env_file=ENV_FILE))
assert cfg_2.dict() == cfg.dict()
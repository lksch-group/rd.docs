from pathlib import Path

from confz import ConfZ, ConfZFileSource
from pydantic import SecretStr, AnyUrl

FILE_PATH = Path(__file__)
ENV_FILE = FILE_PATH.parent / "confz_env.yml"


class DBConfig(ConfZ):
    user: str
    password: SecretStr


class APIConfig(ConfZ):
    host: AnyUrl
    port: int
    db: DBConfig

    CONFIG_SOURCES = ConfZFileSource(file=ENV_FILE)


def test_config():
    cfg = APIConfig()
    assert cfg.port == 1111
    assert cfg.db.user == "admin"
    print(cfg)
    print(cfg.json(indent=2))
    # every model has a schema, too!
    print(cfg.db.schema_json(indent=2))

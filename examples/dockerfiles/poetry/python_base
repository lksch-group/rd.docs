# Creating a python base with shared environment variables
ARG PYTHON_VERSION=3.10.7
FROM python:$PYTHON_VERSION-bullseye as python-base
ARG POETRY_VERSION=1.1.13
ARG DEV_UID=1000

# The env vars are inherited in all images that use FROM python-base as ....
# OSTYPE has be to set since it is normally there in linux and used by /venv/bin/activate
ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_VIRTUALENVS_IN_PROJECT=false \
    POETRY_NO_INTERACTION=1 \
    POETRY_VERSION=$POETRY_VERSION \
    OSTYPE='linux-gnu'

# Setting up proper permissions, running app not as root
# install dependencies for your application, update system
RUN mkdir -p /app \
    && groupadd --gid $DEV_UID -r dev \
    && useradd --create-home -d /home/dev --uid $DEV_UID -g -r dev dev \
    && chown dev:dev -R /app \
    && apt-get update && apt-get upgrade -y \
  && apt-get install --no-install-recommends -y \
    bash

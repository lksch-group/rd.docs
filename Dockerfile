FROM python:3.9.10-bullseye as python-base

# Setting up proper permissions, running app not as root
RUN mkdir -p /app \
    && groupadd --gid 1000 -r web \
    && useradd -d /app --uid 1000 -r -g web web \
    && chown web:web -R /app \
    && pip install poetry

ARG BUILD_DIR=/app/site

ENV PATH="/app/.local/bin:$PATH"

WORKDIR /app
USER web

COPY ./poetry.lock ./pyproject.toml ./
RUN poetry install

FROM python-base as development

# Expose MkDocs development server port
EXPOSE 8000

COPY --chown=web:web . .

# ARG cannot be used in CMD, but env vars can
CMD ["poetry", "run", "mkdocs", "serve", "--dev-addr=0.0.0.0:8000"]

FROM development as builder

RUN poetry run python -m pytest examples/
RUN poetry run mkdocs build --strict

FROM nginx:latest as production

COPY --from=builder /app/site /usr/share/nginx/html
#!/usr/bin/env bash

target=${1:-development}
docker build --tag rd_docs:"$target" --target "$target" .
---
title: Dockerfile
tags:
  - Dockerfile
---

{% extends 'page_base.md' %}

{% block content %}

## Dockerfile definition

The [Dockerfile]({{ urls.dockerfile_ref }}) is a means of defining an application container using instructions like `RUN` or `COPY`. Each instruction 
creates a layer on top of the previous one. This way, a docker build can be cached by layers to speed up build times. 

## Running a docker image

When you do `docker run <repo/image:tag>`, the following happens

- The container is started using the last `USER` defined in the Dockerfile (if any)
- If a `CMD` was specified in the `Dockerfile`, runs this command using `PID 1`, which is the main process of the container

## Building containers

When creating your own containers, be aware of the following:

- **Fewer instructions means a smaller the resulting container**
- Bake **all dependencies into your container** (e.g. do not install dependencies in a run command)

### For development

If you are using docker for development of applications that require a few other services to function, like web apps, consider the following

- **Create a user inside the container that has the same uid as you** (MacOS and Linux it is 1000 in most cases).
- For an app with a dependency file, **copy the dependency file first**, then **install the dependencies**, last **copy the code**
- Use **[multistage builds]({{ urls.dockerfile_multistage }}) for production** if possible

See also [my take on multistage Dockerfile using python poetry](python_poetry/#complete-example) or an [example for mkdocs website](../python/mkdocs/).

{% endblock %}

---
title: git cheatsheet
tags:
  - git
  - cheatsheet
---

# git cheatsheet

Create a git repository

    git init

## config

**Configure git** globally

    git config --global set user.name "First Last"
    git config --global set user.email foo@bar.com

## clone

Clone a repo using ssh (**recommended for private repos**)

    git clone git@gitlab.com:user/repo.git

Clone using https:

    git clone https://gitlab.com/user/repo.git

## branches

Fetch a remote branch

    git fetch origin branchname

Push branch to the remote and create its origin

    git push -u origin feature

Then, check out this branch

    git checkout branchname

Rename a branch that is currently checkout out

    git branch -m new_name

## rebase

On a feature branch `feature` originating from `main` branch, you can rebase like

    git checkout feature
    git rebase main

## rm

Sometimes you stage files accidentally when they are not yet added to `.gitignore`. Remove those files from 
being tracked **not deleting them**:

    # Folder
    git rm -r --cached .idea

    # File
    git rm --cached .env

## reset

Reset the last 2 commits

    git reset HEAD~2

Then, when you want to also revert changes of these commits (if the file existed, else it is unstaged):

    # for all files    
    git checkout .

    # for a specific file
    git checkout path/to/file

## tags

Tags are handy to mark releases of your software.  

Push tags to remote

    git push --tags

Delete a remote tag:

    git push --delete origin tagname

Delete a tag locally:

    git tag -d tagname

Make a tag

    git tag tagname

## remote urls

See current remote urls

    git remote -v

Set a new remote url

    git remote set-url origin https://gitlab.com/user/repo.git

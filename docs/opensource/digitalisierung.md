---
tags:
 - Politik
 - Digitalisierung
---
# Nachhaltige Digitalisierung?

Wir sehen schon auf eine 30 Jahre lange Internet-Reise zurück und können anhand dessen, 
was wir in den letzten Jahren lesen konnten uns ein Bild der aktuellen Lage verschaffen. 
Die Historie kurz gefasst: Das Internet wurde dank Linux gross, die grössten Digitalkonzerne durch ihre Produkte und Datenschätze. 
Und heute verdienen eben diese Konzerne im Cloud-Geschäft mit Linux sehr viel Geld. 
Die Frage lautet: Was bedeutet "nachhaltig" in der Digitaliserung? Wie wäre das aus Sicht unserer Wirtschaft zu verstehen und wie aus Sicht der Konsumentin, des Konsumenten?

Fangen wir damit an, was **nicht nachhaltig** für die Gesellschaft:

- **Das Übertragen der Verantwortlichkeit und Haftung auf den Nutzer / die Nutzerin** in Nutzungs- und Lizenz-Verträgen. Beispiel: [Tesla-Fahrer müssen in der EU gegen das Gesetz verstossen müssen, um den vollen Funktionsumfang zu nutzen](https://www.zdf.de/politik/frontal/datenkrake-tesla-das-auto-als-spion-102.html).
- **Keine Garantien für Software**. Beispiel: [EULA Samsung Smartphone](https://chaos.social/web/statuses/106845480408555023).
- **Verhinderung des [Right-to-Repair](https://de.wikipedia.org/wiki/Reparatur#Recht_auf_Reparatur)** von elektronischen Geräten wie es [Apple seit Jahren tut](https://yewtu.be/watch?v=9-NU7yOSElE) sowie alle anderen Hersteller auch: Einkleben/Festlöten von Batterien, Festplatten, RAM und weiteren Teilen, oder das Verkleben der Gehäuse von Smartphones.
- **Aufzwingen von strikten Anforderung der Software** an Hardware wie es Microsoft mit Windows 11 tut, was dazu führt, [dass Millionen von Geräten früher als nötig ausrangiert werden](https://www.t-online.de/digital/software/id_90371830/ex-windows-chef-kritisiert-microsoft-fuer-windows-11-systemanforderungen.html).
- **Nutzer sind das Produkt, auch wenn sie für das Gerät bereits bezahlt haben**: Nach dem Kauf akzeptiert man im zweiten Schritt die Nutzungsbedingungen und den Datenfluss. Die dritte Stufe wurde bereits eingeleutet: Die Einbindung der Geräte in autonome Netzwerke, die sich der Kontrolle des Nutzers komplett entziehen ([Amazon Sidewalk](https://en.wikipedia.org/wiki/Amazon_Sidewalk) wurde im Juni in den USA eingeführt. Damit kann dann z.B. Amazon deine Internetleitung für anderen Kunden verwenden.)
- **Schaffung eines Billig-Lohn-Segments (mit AI als Arbeitgeber)**:
    - Velokuriere, die automatisiert durch die App Bestellungen erhalten und ausliefern mit einem Stundenlohn von [Netto 10 CHF pro Stunde](https://www.srf.ch/news/wirtschaft/plattform-oekonomie-sicherheitsluecken-und-hungerloehne-bei-uber-eats) ohne Unfallversicherung da nicht angestellt.
    - **Vermieten von Arbeitskraft im Minutentakt**: [MechanicalTurk](https://www.mturk.com/worker), [Clickworker](https://www.clickworker.com/clickworker-job/) und tausende mehr. 
- **Platform-Ökonomie**: Viele dieser Platformen verstehen sich nur als technologische Vermittler und weisen jede Haftung und damit jedes Risiko (für Investoren) von sich. Beispiel: Ich habe die Nutzungbedingungen der [Design-Platform RedBubble](https://www.redbubble.com/) analysiert. Das Geschäftsmodell besteht unter anderem darin, Verletzungen von Copy-Rights auf Designs auf die Uploader zu schieben. Beim Verkauf verdient RedBubble dennoch, und sichert sich selber eine weltweite, uneingeschränkte Lizenz auf das Design, das vorderhand durch einen Verstoss auf die Platform gelangt sein kann.

## Unternehmensstrategien

- **Wenn Strafen für Gesetzesverstösse geringer sind als die Profite, ist es ein Geschäftsmodell**. Die Facebook-Skandale der letzten Jahre z.B. hatten kaum negativen Einfluss auf den Aktionkurs von Facebook. [Amazon hat kürzlich die höchste je gesehenene Strafe von 740 Mrd. Euro zahlen müssen](https://www.cnbc.com/2021/07/30/amazon-hit-with-fine-by-eu-privacy-watchdog-.html).
- **Verletzung des Prinzips der [Netzneutralität](https://de.wikipedia.org/wiki/Netzneutralit%C3%A4t)**: Gratis-Daten-Pakete bei Telkom-Providern nur für Apps wie Facebook ist insbesondere in Schwellenländern zu beobachten (siehe Facebook-Skandal und Rolle bei der Verfolgung der Rohinjas in Bangladesh)
- **Verlagerung in die Cloud**: Microsoft Office 365, Amazon und Microsoft stellen PC's in der Cloud zur Verfügung. 
- **Internationale Standards selbst festlegen** können (z.B. bei Browser-API's hat Google mit Chrome die führende Rolle)
- **Auslagerung von AI auf die Endgeräte**. Dies spart Strom- und Infrastruktur-Kosten. Die Endgeräte werden zu einem wichtigen Teil des Netzerkes, welches die AI trainiert. Google baut dafür jetzt schon spezialisierte Chips für Smartphones.
- Innovative **Firmen und Start-Ups aufkaufen oder vom Markt drängen**
- Aufkauf kritischer Internet-Infrastruktur (z.B. Kauf von Github und npm durch Microsoft)

## AI als treibende Kraft zu mehr Zentralisierung?

Wer wird das Rennen gewinnen? Die Firmen mit den meisten Daten. Diese Daten werden auch genutzt, um die erfolgversprechendsten Produkte zu identifizieren. Dies ermöglich nicht nur den gezielten Aufkauf, sondern auch Zensierung von Mitbewerbern auf der eigenen Platform (z.B. Facebook Postings über Mastodon werden als Spam behandelt) oder dem Promoting von eigenen Diensten (siehe Google Suche und Strafen durch die EU). 

Diese Art von Digitaliserung ist am Ende auch nicht nachhaltig für Tech-Startups und schwächt die Souveränität unseres Landes weit mehr als ein Rahmenabkommen mit der EU es jemals tun würde. Die grossen Oligopolisten schwächen die sog. 4 Säule der Demokratie - die Medien - und zentralisieren den Informationsfluss mehr und mehr. Wir können uns nicht darauf verlassen, dass diese Konzerne einmal in guter alter Tradition aufgespaltet werden ([Standard Oil Company](https://de.wikipedia.org/wiki/Standard_Oil_Company#Entflechtung)), auch wenn darüber im Kongress in den USA schon diskutiert wurde. Vielmehr dienen sie den politischen Zielen der USA selbst und stehen in diversen Verbindungen zu den Geheimdiensten sind damit Teil des militärisch-industriellen Komplexes ([Palantir](https://de.wikipedia.org/wiki/Palantir_Technologies) mit Bindeglied [Peter Thiel](https://de.wikipedia.org/wiki/Peter_Thiel) zu [Facebook](https://de.wikipedia.org/wiki/Facebook), [Clearview AI](https://de.wikipedia.org/wiki/Clearview_AI) als Beispiele). 

Eine weitere grosse Frage ist die Verteilung der Steuereinnahmen, dann wenn ein Prozentsatz der Jobs durch AI oder durch die Roboterisierung ersetzt werden können, verlagern sich da die Steuern in einer Form, die für den Staat nicht als wünschenwert sind. Der [Deep Technology Podcast](https://www.deeptechnology.ch/lars-thomsen/) widmet sich u.a. diesem Thema mit dem Zukunftsforscher Lars Thomson.

## Marktmacht von Alphabet Inc.

Wir befinden uns heute in genau dieser nicht nachhaltigen Situation, wo diese Bedinungen aufgrund der enormen Marktmacht akzeptiert werden. 
Wir finden Apple, Alphabet (Google), Microsoft in den [Forbes Global 2000](https://de.wikipedia.org/wiki/Forbes_Global_2000#2021), 
der Liste der grössten börsenkotierten Unternehmen der Welt. [Saudi Aramco](https://de.wikipedia.org/wiki/Saudi_Aramco), 
der grösste Erdölkonzern der Welt macht weniger Umsatz und Gewinn pro Jahr als Apple. Erstaunlich!

Ich möchte hier das Beispiel [Alphabet](https://de.wikipedia.org/wiki/Alphabet_Inc.) anführen und aufzeigen, wie allumfassend dieser Konzern geworden ist.

- **Google Suche**: Die Google Suche berücksichtigt über 100 Parameter bei der Suche, und soll dir genau das liefern was du suchst. Die Resultate sind nicht die gleichen wie bei deiner Nachbarin, die dasselbe eingibt. Sie hat riesigen Einfluss darauf, **wer gefunden wird und wer seine Produkte verkaufen kann.**
- Google ist mit **Android** auf mit einem [Marktanteil 2021 von ca. 70%](https://de.wikipedia.org/wiki/Android_(Betriebssystem))
- Google verlangt **30% Provision auf alle sog. In-App-Käufe**, die über die jeweilige Bezahlschnittstelle des Playstores laufen muss
- Google betreibt den **Android-Store** (Google Play Services) und weiss damit z.B.
    - genaue **Download-Zahlen** von Apps und welche am erfolgreichensten sind
    - Wieviel **Umsatz** *in-App* generiert wird
    - **Nutzungstatistiken** jeder App
    - Message und Geolocation Anfragen der Apps laufen über die Google-API
- **Gmail ist Marktführer** für Emails und sämtliche Email-Texte werden gescannt und verwendet
- **Google Maps** wird weltweit am meisten verwendet und gilt als eines der erfolgreichsten Google Produkte überhaupt
- Gemäss Schätzung einer Studie beantwortet **Google DNS** 30% der weltweiten DNS-Anfragen (Artikel auf heise.de).
- Die **Google Cloud Services** zählen mit Amazon und Microsoft zu den Grössten der Welt. Immer wenn etwas digitalisert wird, stellt sich die Frage wo man einen Server mietet...
- Google hat mit **Android Auto** seinen Zugang in die mobilen fahrenden Computer names Automobile erlangt und dominiert hier mit Apple Car Play.
- Google entwickelt den meistbenutzten **Browser Chrome** mit Marktanteil von ca. 65%, der Google sehr viel über die Nutzer verrät
- Google besitzt **Youtube**, da wo weltweit nach Google am meisten Suchanfragen eingegeben werden, entwirft den Algorithmus, und beeinflusst durch diesen auch die Machart und Themen der Videos.
- Google bietet das **"Login mit Google"** und fungiert so als globaler Authentifizierungsprovider, womit dann Google über die Nutzung von Drittdiensten bescheid weiss
- Google kann mit den **Google Captchas** seine AI für Autos trainieren, dies gratis durch die Klickarbeit von Millionen von Nutzern
- Bei jeder Website, die **Google Fonts** verwendet, kriegt Google Infos, welche Seite der Nutzer ansteuert
- Der Alphabet Konzern hat ausserdem ein Infrastruktur-Bereich Google Fiber und sogar eine Firma namens [Calico, die sich der Erforschung der Unsterblichkeit widmet](https://www.sueddeutsche.de/wirtschaft/google-das-c-im-alphabet-1.2607421).

**Fazit**: Google hat für Forschung und Entwicklung so viele Mittel zur Verfügung, dass selbst Firmen in spezialisierten Domainen Schwierigkeiten haben, 
mit den Innovationen mitzuhalten. Beispiel: Kein Navigationsgeräte-Hersteller kann mit der Genauigkeit und Aktualität der Google Navigation mehr mithalten. 
Oder: Google investiert mehr Geld in das autonome Fahren als die gesamte deutsche Autoindustrie zusammen.

## Ein Plädoyer für mehr Paranoia

Es gibt ein grosses Paradox: Einerseits existiert eine Tendenz zu einer [0-Risiko-Gesellschaft](https://www.srf.ch/play/tv/sternstunde-philosophie/video/rafaela-hillerbrand---ethik-des-risikos?urn=urn:srf:video:cf0e1b2f-a121-4d6f-82a3-133c4a58d37c). Das **Sicherheitsbedürfnis** hat sich erhöht, was man auch mit Wohlstand und technologischem Fortschritt assoziieren. Diese Sichheit ist aber sehr trügerisch. Denn unseren Gesellschaft ist durch die Vernetztheit nicht widerstandsfähiger geworden. Wir bewegen uns gerade sowieso in eine sehr unsichere Welt hinein, beobachten ein **technologisches Wettrüsten**, einen globalen Streit um die **Dominanz bei der 5G-Technologie** und völlig **mangelde und hinterherkindende gesetzliche Rahmenbedinungen** und **keine Steuergereichtigkeit**. Wir sollten aufpassen, welche Monster wir füttern und die Kabel kappen, bevor digitale Alpträume durch technologische Dominanz in einem repressiven politischen Umfeld in Zukunft Wahrheit werden können. Denn am Ende ist die Wirtschaft der Motor, der die Überwachung bringt, und die (totalitären) Staaten sind die Gesetzgeber, zu deren Zusammenarbeit die Tech-Konzerne verpflichtet werden. Und wir füttern das System weiter, obwohl es im digitalen Bereich so viel einfacher ist, auf Alternativen umzusteigen resp. diese zu erschaffen (siehe z.B. das [Fediverse](https://www.kuketz-blog.de/das-fediverse-social-media-losgeloest-von-den-fesseln-kommerzieller-interessen/)).

## Links

- [Wir hacken Deutschland](https://yewtu.be/watch?v=xYlW39ErR1M)
- [Wie Apple sein eigenes Werbenetzwerk ausbauen will und dabei geschickt Marketing betreibt, um davon abzulenken](https://yewtu.be/watch?v=YnaBEiBVngw)
- [Leben ohn Google: GEHT DAS?](https://www.zdf.de/funk/somanytabs-12189/funk-leben-ohne-google-geht-das-alternativen-fuer-gmail-chrome-maps-mit-mrwissen2go-102.html)

{% include 'glossary.md' %}
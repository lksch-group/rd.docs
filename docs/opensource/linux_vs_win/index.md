---
title: Linux v.s. Windows
description: Vergleich zwischen Windows und Linux mit verschiedenen Kriterien
tags:
  - Linux
  - Programme

programs:
  - 
    - Kategorie
    - Windows
    - Linux
  - 
    - '-'
    - '-'
    - '-'
  - 
    - Vektorgrafik
    - (Illustrator/CorelDraw)
    - Inkscape
  - 
    - Bildbearbeitung
    - (Adobe Photoshop)
    - GIMP
  - 
    - PDF-Reader
    - Adobe u.a.
    - Okular
  - 
    - Office
    - Libre Office oder OnlyOffice (MS Word)
    - "LibreOffice oder [OnlyOffice](https://www.onlyoffice.com/de/)"
  - 
    - Mail
    - Outlook & Thunderbird
    - Thunderbird
  - 
    - Fotos
    - (Adobe Lightroom)
    - gThumb / Darktable
  - 
    - Backup
    - "[Cobian Backup](https://www.cobiansoft.com/cobianbackup.html)"
    - Timeshift / DejaDup
  - 
    - Video
    - "[VLC](https://diode.zone/w/kXuNxBdJVKuRePquDMkBYM)"
    - "[VLC](https://diode.zone/w/kXuNxBdJVKuRePquDMkBYM)"
  - 
    - Webcam
    - ???
    - "[Cheese](https://wiki.gnome.org/Apps/Cheese)"
  - 
    - Text-Editor
    - Notepad++
    - Notepadqq
  - 
    - Audio Editing
    - Audacity
    - Audacity
  - 
    - Digital Audio Workstation (DAW)
    - Reaper
    - Ardour 6
  - 
    - Musik-Notenprogramm
    - MuseCore
    - MuseCore
  - 
    - Digitale Gestaltung
    - ???
    - "[Krita](https://krita.org/en/)"
  - 
    - Notizenapp
    - ???
    - "[Joplin](https://joplinapp.org/)"
  - 
    - Festplatten-Manager
    - DiscManager (built-in)
    - GParted
  - 
    - Schriften
    - ???
    - Font Manager

---

Die folgenden Punkte basieren auf den Eindrücken, die der Autor seit 2016 mit den Distributionen **Ubuntu**, **Linux Mint** und **Pop!OS** auf verschiedenen Geräten gemacht hat. 2021 gibt es viele Linux Distributionen, der für den Alltagsgebrauch mehr als nur geeignet sind. Ebenso evident ist, dass Windows die guten Funktionen von Linux und Mac OSX immer erst viel später liefert und man bei Linux schneller mit neuen, gut durchdachten Lösungen rechnen kann. Man hat also nicht nur mehr Wahl sondern auch schnellere Entwicklung.

Für den Einstieg zu empfehlen sind:

- [Pop!OS](https://pop.system76.com/) basierend auf Ubuntu mit Gnome Desktop
- [Linux Mint]({{ urls.linux_mint }}) gut für den Umstieg, basierend auf Ubuntu mit Cinnamon oder Mate Desktop
- [Elementary](https://elementary.io/) ist dem Design nach Mac OS X nachempfunden

## App Store

Schon bevor Windows 10 endlich einen App-Store brachte, gab es dies bei Linux schon seit den 90er Jahren (apt & rpm). Bei den meist genutzten Linux Distros gibt ebenfalls einen App Store. Seit dem Aufkommen der Smartphones hat man sich bereits an App Stores gewöhnt.


<figure>
  <img src="../../img/pop_shop.png" />
  <figcaption>App Store in Pop!OS</figcaption>
</figure>

## Sicherheit

Linux ist aus folgenden Gründen als sicherer zu betrachten:

- unter Linux (und MacOS) ist der Nutzer kein Admin-Nutzer (siehe [Unterschiede Nutzerrechte](../linux/basics/nutzerrechte/))
- Programme werden i.d.R. aus Paketquellen installiert. Diese sind signiert sind und damit vertraulicher als irgendein Programm, das man sich für Windows auf dem Internet herunterlädt.
- Kriminelle investieren mehr Zeit in Windows, da es mehr Nutzer gibt. Auf Linux braucht man daher auch nicht unbedingt ein Anti-Viren-Programm.

## Integration von Online-Accounts

In der Regel möchten wir ein System, wo unsere Kontakte, der Kalender, die Email-Software, Notifications miteinander integriert sind. 

<figure>
  <img src="../../img/gnome_online_accounts.png" width="500" align="right" />
  <figcaption>Account Integration Gnome Desktop</figcaption>
</figure>

## Suchfunktion

Viele Benutzer suchen nach Programmen und Einstellungen mithilfe einer globalen Suchfunktion.  
Bei Window 7 war diese zum ersten Mal integriert, hat sich dann unter Windows 10 mit Einführung der Apps neben den Programmen verschlechtert. Für gewisse Programme musste man extra eine Verknüpfung auf dem Desktop anlegen, damit das Programm durch die Suche gefunden wurde. Insgesamt war ich sehr enttäuscht, dass ein so grosser Konzern wie Microsoft dies nicht besser hinbekommt. Mittlerweile haben sie dies wohl behoben und man findet auch Systemeinstellungen gut.

Mit **Linux Mint** hatte ich das erste Mal 2017 eine Alternative gesehen. Bei Mint und Pop!OS ist die Suche tadellos. In **Pop!OS** wird wie bei MacOS das gesamte System durchsucht: Files, Programme, Kontakte, alle Einstellungen und sogar der App Store, sofern ein Programm gesucht wird, das noch nicht installiert ist.

Nach mehreren Jahren des Vergleichs kann ich sagen: Alle Linux-Distros haben Windows bei der Suchfunktion geschlagen.

## DarkMode und NightLight

Heute können Browser beim Surfen die Einstellung einsehen, ob das Betriebssystem im sog. DarkMode ist. Webseiten erkennen diesen Modal manchmal und stellen sich darauf ein. Der **Nachtlicht-Modus** ist besonders praktisch, um die Augen zu entlasten. Ebenso kennt man es von Smarthpones, dass sie eine Funktion anbieten, den Rotlicht-Anteil im Bildschirmlicht abend zu erhöhen, damit unser Körper sich besser auf das Schlafengehen einstellen kann.

<figure>
  <img src="../../img/os_dark_mode.png" />
  <figcaption>Dark Mode Gnome Desktop</figcaption>
</figure>

<figure>
  <img src="../../img/night_light_gnome.png" />
  <figcaption>Nachtlicht Gnome Desktop</figcaption>
</figure>

## Windows Tiling Funktion

Die Tiling-Funktion von Fenstern ist Bei Pop!OS bereits miteingebaut. 
Fenster werden auf ihre maximale Grösse gerbacht, was mühsames Grossziehen und Herumschieben obsolet macht. 
Dabei spart man jeden Tag sehr viel Zeit. Im Gnome Desktop kann man sich unabhängig von der Distro den [Tiling Assistant](https://github.com/pop-os/shell) herunterladen. 
Dieses Feature wird bei Windows 11 mit geplantem Start im Oktober 2021 dann auch vorhanden sein.

## Peripherie

### DualScreen Setups

Immer wieder Probleme bereiten Dual-Screen-Setups, besonders wenn gepaart mit 4k Bildschirmen, die in voller Auflösung fast nicht lesbar sind, und für die man dann Skalieren muss, damit die Schrift wieder lesbar wird.  

Auch Probleme gibt es beim Ausstecken des einen Displays und wie die Fenster auf dem verbleibenden Bildschirm angeordnet werden.

Ungefähr 2018 wurden diese Features in Ubuntu / Mint soweit verbessert, dass ich sehr gut damit arbeiten konnte mit unterschiedlichen Auflösungen und Scaling vom 4K Bildschirm und ohne weiteren Aufwand.

In 2020 hatte ich das gleiche Setup mit Pop!OS, was nahtlos funktionierte. **Die Fenster werden wieder auf den zweiten Bildschirm verteilt, sobald man ihn einsteckt**.

<figure>
  <img src="../../img/gnome_display_settings.png" width="500" />
  <figcaption>Dual Screen Beispiel in Gnome Desktop</figcaption>
</figure>

### Drucker

Es gibt einige grosse Hersteller, wie Brother, die Treiber für Drucker anbieten. Ausserdem wird seit Debian 11 treiberloses Drucken unterstützt. Die Erfahrung bei der Einrichtung eines Drucker/Scanner-Gerätes von Brother über WLAN hat problemslos funktioniert. Der **[Gnome Document Scanner](https://roytanck.com/2020/10/13/an-ode-to-gnomes-document-scanner/)** App ist eigentlich fast perfekt. Ich war überrascht, den Drucker in allen Optionen einstellen zu können, ohne dabei ein Programm zusätzlich installieren zu müssen.

## Gaming

Steam im App Store holen, loslegen! Valve, die Betreiberin von Steam hat seit 2018 viel Zeit und Geld in Linux-Gaming investiert. AMD Grafikkarten-Treiber müssen unter Linux nicht installiert sind, da AMD ihre Treiber in den Linux Kernel miteinpflegt. Nvidia Treiber sind für Linux vorhanden und unterstützen auf moderne Technologien wie DLSS. Die Steam App auf Linux bietet den Kompatibilitäts-Modus an, womit auch Windows Games gestartet werden können. Dies ist mit wenigen Klicks in den Game-Settings einstellbar. Ebenso sind fast alle Game-Pads unterstützt. **Du kannst damit rechnen, dass ca. 85% deiner Games auf Linux spielen kannst** ([Lutris Demo](https://yewtu.be/watch?v=hCQDAXyNkCo)).

Coole FREE Games zum Anfangen:

- [0 A.D. (Age of Empires Clone)](https://play0ad.com/)
- [Xonotic Arena Shooter](https://xonotic.org/)
- [SuperTuxCart](https://supertuxkart.net/Main_Page)
- Open Arena (Quake3 clone)

## Programmvergleich

Hier eine kleine Übersicht, welche Programm man so typischerweise von Windows kennt mit dem Pendant für Linux oder auch solche, 
die aus dem OpenSource Universum kommen und auch auf Windows verfügbar sind. Programme, die etwas kosten, sind in () gesetzt.

{% set table = programs %}
<div markdown="1" class="programs-table">
{% include 'table.md' %}
</div>

Hier eine Auswahl an Programmen, die mit dem System/Desktop als Standard mitkommen in PopOS / Gnome Desktop:

- Kontakte
- Kalender
- PDF Document Viewer
- PDF Document Scanner
- Screenshot Software (analog zu SnippingTool)
- USB Flasher
- Media Sharing, Screen Sharing und Remote Login

!!! tip
    Mit dem Programm [bottles](https://usebottles.com/) gibt es die Möglichkeit, Windows Programme auf Linux laufen zu lassen.

## Updates

Die Update unterscheiden sich vor allem in Häufigkeit und Grösse (auf Linux sind sie i.d.R. kleiner und etwas häufiger). In den oben erwähnten Distributionen ist der **Update-Ablauf über den Desktop/App-Store mit einem Klick** möglich und daher genau so einfach wie bei Windows. Auch wird der Nutzer über neue Updates per **Notifications** auf dem Desktop aufmerksam gemacht. 

Sofern man Angst hat, dass heutzutage bei Linux Updates etwas kaputt geht: Der Autor hatte in 4 Jahren auf mehreren Geräte und kann sich weder an Systemabstürze noch kaputtgegangene Programme erinnern. 

## Upgrades

Ein Upgrade ist in dem Sinne zu verstehen, dass bei einem Upgrade das Betriebssystem auf eine neue Version gebracht wird. Dabei werden meist neue Funktionen mitgeliefert. Bei den verschiedenen OS sind diese Versionen unterschiedlich bezeichnet. Beispiele sind:

- **MacOS**: z.B *SnowFlake* oder *Catalina*
- **Windows**: z.B. *Windows 10 Profession Version 20H2*
- **Linux**: z.B. *Linux Mint 20.2 "Uma"* (bedeutet herausgegeben Februar 2020)

Bei Linux und MacOS werden die Upgrades dem Nutzer transparent angekündigt. Bei Windows ist das nicht immer der Fall. Ob es sich um ein Update oder um ein Uprade handelt, ist nicht immer klar. Auch ist es nicht immer so, dass die Updates von Windows problemlos durchlaufen.

## Links

- [The Rise Of Open-Source Software](https://yewtu.be/watch?v=SpeDK1TPbew)
- [Why Microsoft keeps beating Apple and Google with Windows](https://yewtu.be/watch?v=OcL932W6E1w)
- [Linux User vs Windows User](https://yewtu.be/watch?v=DoOyxbyY7ww)
- [Is Windows bad?](https://yewtu.be/watch?v=Sh1kZD7EVj0)
- [Die Macht der Superreichen: Bill Gates](https://www.zdf.de/dokumentation/zdfinfo-doku/die-macht-der-superreichen-bill-gates-100.html)

{% include 'glossary.md' %}

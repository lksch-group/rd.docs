---
title: Links
tags:
 - Linux
 - Local Community
---

- [Linux und Opensource Treff](https://openki.net/course/iCSJJJ8ZhTkJshJ6a/linux-und-open-source-treff)
- [Mike Kuketz's Empfehlungstrecke](https://www.kuketz-blog.de/empfehlungsecke/)
- [Linux User Group für sehr fortgeschrittene Nutzer](http://www.lugs.ch/)
- [Media CCC](media.ccc.de/)

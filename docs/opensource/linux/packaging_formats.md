---
title: Flatpak, Snap und AppImages
tags:
  - Programme
  - Linux
  - AppImage
  - Flatpak
  - Snap
---

{% extends 'page_base.md' %}
{% block content %}

# Flatpak, Snap und AppImages

Nicht erstaunlich dass es bei der Vielzahl von Linux-Optionen auch **verschiedene Arten gibt, Programme zu 
installieren**. Die folgenden Lösungen bringen in Vergleich zu einer **klassischen Installation mit dem Paket-Manager** 
den Vorteil, dass diese Formate ihre Abhängigkeiten mitliefern und sie **gekapselt vom Rest im System** laufen. 
Das hat den Vorteil, dass bei der **Vielzahl der Distributionen die Verteilung von Programmen viel einfacher wird**, da 
man das nur einmal machen muss statt für alle Linux-Distributionen separat. Damit steigt dann auch die Verfügbarkeit 
von Programmen, was wir alle begrüssen.

## Übersicht

{% set items =  app_fmts %}
{% include 'tabbed_pros_cons.md' %}

## Flatpaks

Flatpak nutzt sich am besten wenn bereits in den App Store des OS integriert. Ansonsten kann man alle Dinge auch 
über die Kommando-Zeile machen:

```shell
flatpak --help
flatpak list
flatpak install org.chromium.Chromium
flatpak run org.chromium.Chromium
```

Wie man sieht entsprechen die Programm-Namen nicht denen, die man sonst üblicherweise nutzt: `sudo apt install chromium`. 

## AppImages

!!! Appimaged tip

    [AppImageD]({{ urls.appimaged }}) macht es einfacher, appimages automatisch zu installieren, indem es einen 
    Desktop-Eintrag erstellt.

## Snap's

Der [Snap Store]({{ urls.snap_store }}) wird von Canonical betrieben, die Firma, die hinter Ubuntu steht. 
Viele Nutzer sagen, sie würden es nicht begrüssen, dass hier Ubuntu eine Monopol-Strategie fährt und stehen diesem Format 
teilweise ablehnend gegenüber. Ist es in erster Linie für ein Server Betriebssystem entwickelt worden, und hat einige Vorteile 
gegenüber den anderen Formaten, wie in der Übersicht erwähnt. Ich würde es dann verwenden, wenn ein Programm nicht im 
Paket-Manager existiert und man es per Kommando-Zeile aufrufen will. Sofern snap nicht installiert ist, kann man es nachinstallieren:

```shell
sudo apt install snapd
```

## VSCodium eher als snap

Ich habe bislang auf snaps verzichtet und konnte fast alle Bedürfnisse mit Flatpaks decken (auf Pop!-OS, Derivat von Ubuntu). 
Es gab aber Gelegenheiten, wo ich mich für ein `snap install` entscheiden würde.

Bei **VSCodium** handelt es sich um eine ent-telemetrisierte Version von **VSCode**, dem weit genutzten Code-Editor, 
der von Microsoft entwickelt wird. Auch diesen Editor gibt es nur noch über Snaps in Ubuntu. In diesem Fall nutze ich das snap package

- Das integrierte Terminal funktioniert nicht richtig bei Nutzung mit flatpak
- `codium .` funktioniert mit flatpak install nicht

## Referenzen

- [Snaps, Flatpaks and AppImages Do Very Different Things!]({{ urls.distrotube_flatpak_snap }})


{% endblock %}

---
title: Wahl Linux-Distro
description: Eintscheidungkriterien für die Auswahl einer Linux-Distribution
tags:
  - Linux
  - Distro
---

# Wahl der passenden Distribution

Eine Distrubution besteht immer aus:

1. Kernel, tiefster Unterbau des Systems
2. Paketquellen der Distribution
3. Desktop-Umgebung

## Faktoren

- Alter und Art des Gerätes
- Anwendungszweck

## Gerätempfehlung

Folgende Marken gelten bei Linux als gut untersützt, weil auch viele Entwickler mit solchen Geräten arbeiten:

- IBM/Lenovo Notebooks
- Dell
- HP

## Distributionen ausprobieren

Das Tool [Ventoy](https://www.ventoy.net/en/index.html) erlaubt es, einen USB-Stick mit verschiedenen Distributionen zu erstellen und diese dann im Live-Mode zu booten und auszuprobieren.

!!! Ventoy warning 

    Nicht alle OS können zuverlässig mit Ventoy gestartet werden. Im Zweifelsfall einfach ein OS auf einen Stick.

## Grundsatz-Empfehlungen

Für Anfänger oder auch alle anderen, die sich unsicher sind, folgende Empfehlung:

1. Eine Distro nutzen, die weit genutzt wird und entsprechend solid und gut gewartet wird.
2. Eine Distro, wo man [AppImages, Flatpaks und Snaps](/opensource/linux/packaging_formats) nutzen kann, um eine **breite Palette an Programmen zu haben**


## Grafik-Karten

Bei **Nvidia**-Grafikkarten müssen sollten die proprietären Treiber von Nvidia installiert werden. Für **AMD** Grafikkarten 
sind die Treiber im Linux Kernel mit dabei.

## Empfehlung

Ich empfehle eine **Debian** resp. **Ubuntu** basierte Distribution. Wer von Windows kommt, kann sehr gut mit **Linux Mint** starten. **Ubuntu** selber kommt mit dem **Gnome Desktop**, den ich selbst sehr schätze. Eine weitere, sehr gute Desktop-Umgebung ist **KDE**. Am besten schaut man sich einmal [einen Vergleich zwischen den beiden an](https://yewtu.be/watch?v=32gyFIWecuw).

- [Mint]({{ urls.linux_mint }})
- [Fedora]({{ urls.fedora_home }})

Der unten verlinkte Video vom Channel *The Linux Experiment* fasst alles ziemlich gut zusammen. Er vergleicht die folgenden 
Distros:

- Ubuntu
- Linux Mint
- elementary OS
- Arch Linux
- Solus
- Fedora
- Deepin
- Manjaro
- Debian
- PopOS
- Gentoo
- ZorinOS
- KDE Neon

## Links

- [Ranking Linux Distributions for 2023: not your average tier list!](https://yewtu.be/watch?v=d7-EhGIeGUs)
- [Snaps, Flatpaks and AppImages Do Very Different Things!]({{ urls.distrotube_flatpak_snap }})
- [Major Distributions DistroWatch](https://distrowatch.com/dwres.php?resource=major): Welche Distributionen sind gerade am beliebtesten. Diese Statistiken sind aber nicht wirklich repräsentativ, da niemand alle diese Daten zentral zusammenträgt.

{% include 'glossary.md' %}

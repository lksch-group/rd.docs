---
title: Password Managers
tags:
  - Sicherheit
  - Security
  - Password
---
{% extends 'page_base.md' %}
{% block content %}

It's a good idea to have a **password manager** with a **main password** to access the rest of them. Here is my recommendation 
that **fits following needs**:

- [x] Not one that is provided by a browser
- [x] Does have a browser extension
- [x] Runs on all platforms, also Linux
- [x] Uses a local password database that can be synced using a sync tool of my choice
- [x] is FOSS

## [KeepassXC]({{ urls.keepassxc }})

For the criteria above there is only Keepass XC that matches them all. It's really a good choice, not perfect, but 
has a lot of more advanced features as well. It has a [browser plugin for Firefox]({{ urls.keepassxc_firefox }})

### Setup

1. **Download** [keepass]({{ urls.keepassxc_dl }})
2. **Create a password database**. Use a master password that is long, e.g. **a combo of different words separated by spaces**
3. Go to the **Settings -> Browser Integration** and :material-checkbox-outline: your browser of choice
4. Get the **browser plugin**: [Firefox]({{ urls.keepassxc_firefox }}), [Chrome]({{ urls.keepassxc_chrome }})

??? tip "Store ssh keys"

    Keepass is able to store whole ssh private keys with passwords inside the db. With {{ macros.keyboard_shortcut(['ctrl', 'h']) }} 
    they can be added to the ssh agent.

## Resources

- [Have I been pawned?]({{ urls.haveibeenpwnd }})
- [SRF Kassensturz - Password Manager Test](https://www.srf.ch/sendungen/kassensturz-espresso/tests/security-test-diese-passwortmanager-schuetzen-vor-hackern)

{% endblock %}
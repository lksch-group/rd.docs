---
tags:
  - RaspberryPi
---

{% extends 'page_base.md' %}
{% block content %}

# Heimprojekte zum Basteln

## Für Raspberry Pi

- [PiHole](https://pi-hole.net/): Heimnetzwerk-Türsteher, siehe [Anleitung](../raspi/pihole_install/)
- [RetroPi Gaming Konsole]({{ urls.retropie }}): Fun-Retro-Games mit vielen Kontrollern
- [Sonic-Pi](https://sonic-pi.net/): Live-Coding mit Musik
- [PiVPN](https://pivpn.io/): OpenVPN auf Raspi
- Anleitung [PiVPN / Pi-Hole / Unbound](https://pmj.rocks/tutorials/server/eigenes-vpn-mit-pivpn-pihole-und-unbound-in-debian) für Debian
- [MoodleBox](https://moodlebox.net/de/) Lernplatform Moodle fürs Raspi
- [Raspberry Pi Projekte Official](https://projects.raspberrypi.org/de-DE/projects)

Gewisse Projekte lassen sich sinnvoll kombinieren, z.B. PiHole mit PiVpn.

{% endblock %}

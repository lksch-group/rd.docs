---
title: Quereinstieg
---

{% extends 'page_base.md' %}
{% block content %}

# IT-Quereinstieg

Ende 2022 wurde ausgiebig über den **Fachkräftemangel in der IT** berichtet, und dass sich das Problem noch verschlimmern wird.
Da ich selbst 2016 als **Quereinsteiger** in die IT einstieg, möchte ich von meinen Erfahrungen berichten und mit den nachfolgenden Informationen 
eine Hilfestellung oder Orientierung geben. Vorab ein paar orientierende Informationen, wie sich die Lage 2022 gestaltete.

## IT-Landschaft

Die Recherche von Tanja Eder im [SRF Digital Podcast]({{ urls.srf_digital_fachkraeftemangel }}) fand ich spannend und breit recherchiert.
Stand **August 2022** waren **31'000 Stellen offen**, was ca. **10% aller offenen Stellen** entsprach. Weiter fand sie heraus, dass

- doppelt so viele IT-Stellen wie Jobs für kaufmännische Berufe offen sind,
- **2/3 aller IT-Fachkräfte nicht in IT-Firmen** wie z.B. Swisscom arbeiten, sondern Banken, Versicherungen, KMU
- Unis schon weit mehr IT-ler als in den vergangenen Jahren ausbilden, es aber bei weitem nicht ausreicht
- und es weiltweit etwa doppelt soviele **IT-Security-Fachkräfte** bräuchte
- es vor allem auch beim Staat/den Verwaltungen und KMU's an Personal fehlt
- es gibt heute leider nur **15% der IT-Fachkräfte Frauen**

Zahlenmässig am meisten offenen Stellen gibt es für

1. Applikationsentwickler/In
2. IT-Projetkmanager/In
3. IT-Admininistration
4. Datenbankspezialist/In

Obiges bedeutet auch, dass es diverse Rollen gibt, wo IT-Wissen in Kombination mit anderem Wissen (z.B. IT-Projektmanager) wichtig ist. 
Mittelfristig kann jedes KMU jemanden brauchen, der sich gut in IT auskennt. Gut ausgebildete Fachkräfte werden bei spezialisierten IT-Firmen 
bessere Arbeitskonditionen und Löhne vorfinden. 

## Innere Motivation

In Gesprächen mit alten IT-Hasen ist mir eines aufgefallen: Sehr viele sind über **Games zur IT** gekommen. Ob sie normal spielten, 
oder versuchten, am den Gamemechaniken rumzuschrauben. Wenn man es als Kind schafft, den **Computer coole Dinge tun zu lassen**, dann fasziniert das. 
Was ist es, was dich antreibt?

### Persönliche Voraussetzungen

Es gibt gewisse Eigenschaften, die einem Vorteile bringen, wenn man sich auf einen IT-Beruf einlassen will. Diese hängen mit der Natur des Jobs zusammen. 

- **Die IT-Landschaft ändert ständig**. Daher ist eine hohe Motivation zum sich konstant weiterzubilden zu wollen, von Vorteil.
- **Frustrationstoleranz**: Man sollte beim Erlernen und Entwickeln von Dingen davon ausgehen, dass es *a priori* nicht funktioniert, und sich freuen, wenn es funktioniert.
- **Abstraktionsvermögen**: Vielfach kommt man in die Situation, wo man früher oder später technische Systeme erklären oder entwerfen muss. Auch für andere Profile sind Kenntnisse von Mathematik ein Vorteil.
- **Durchaltewillen**: Auf der Durststrecke bis zum ersten Beruf braucht man **Strategien** zum durchalten, um **nicht die Motivation zu verlieren**.

Alle obigen Punkte fallen leichter, wenn man eine **intrinsischer Motivation** hat, Neues zu lernen. 
**Sprachbegabtheit** scheint ebenfalls ein Faktor zu sein gemäss SRF Digital Podcast, und ich kann dies bestätigen. 
Wer gut und schnell Sprachen lernt, und Freude daran hat, dem könnte auch Programmieren gut gefallen. 

## SwissICT Jobprofile

Die **IT ist ein ziemlich weites Feld**, und die [Übersicht der Berufsfelder von SwissICT]({{ urls.ch_ict_jobs }}) kann 
man hier zu Rate ziehen, um sich einen Überblick zu verschaffen. Die Übsersicht zeigt auch, welche **Fähigkeitsprofile in 
der jeweiligen Stelle am wichtigsten** sind. Daher sieht man dort recht gut, was einem vom Profil her erstrebenswert scheint. 

## Eigene kleine Projekte

Suche dir kleine Projekte, die du umsetzen kannst und mit denen du übst. Nur durch **Learning by doing** bekommt man Erfahrung 
und kann sich ein **Portfolio aufbauen**, welches man bei den Bewerbungen zeigen kann. Nimm dazu, was dir die **OpenSource** Welt bietet. Du kannst hier z.B. 
in der [Bastelecke](../../Basteln) vorbeischauen.

## Podcasts und dergleichen

Podcasts haben mich gut durch die Zeit begleitet, indem sie auf die **wichtigsten Änderungen aufmerksam** machen, coole neue **Projekte portraitieren**, 
und somit auch zu einer besseren Übersicht beitragen. Ausserdem inspiriert es, **Neues auszuprobieren**. 
Manche bieten auch eine Community, wo man Fragen stellen kann.

## Referenzen

- [inside-it.ch - IT-Fachkräftemangel nimmt zu]({{ urls.inside_it_fachkraeftemangel }})
- [SRF Digital - IT-Fachkräftemangel]({{ urls.srf_digital_fachkraeftemangel }})
- [Fachkräftemangel auf Höchststand – Prüfen Sie Ihren Beruf!]({{ urls.srf_fachkraeftemangel_test_22 }})
- [Jetbrains Python Developers Survey 2020]({{ urls.jetbrains_dev_survey_20 }})

{% endblock %}

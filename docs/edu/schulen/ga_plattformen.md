---
title: Plattformen
competences:
 - Wie verdienen Plattformen Geld und wem gehören sie
 - Verstehen, welchen Wert Daten haben können, auch mit Blick in die Zukunft (Technikfolgenabschätzung)
 - Verstehen, wie Algorithmen funktionieren und auf welches Ziel hin sie optimiert sind
 - Komplexität des Themas verstehen

---
{% extends 'page_base.md' %}
{% block content %}

In dieser Gruppe beschäftigen wir uns mit **SocialMedia Platformen** und wie diese funktionieren. Wir können uns alle einig sein, 
dass **Social Media** einen **Einfluss auf unsere Gesellschaft** hat und dass unsere **soziale Kommunikation** zu einem Grossteil über 
**Dienste aus privater Hand** passiert, und auch dass wir für **deren Dienste kein Geld bezahlen**. Da es sich meist um private Akteure handelt, gibt es 
für die Wissenschaft und Journalismus **nicht sehr viele Details über die Internas dieser Konzerne**, z.B. wieviel **Geld für politische Werbung** ausgegeben wurde, 
oder mit welchen Partnern man (illegal?) Daten austauscht oder wie die **Algorithmen** der Konzerne funktionieren.

Somit ist dieses Thema sehr **vielfältig und komplex**, auch in Bezug auf die Position des Staates, wie solche Platformen in Zukunft reguliert werden 
sollen. Im Zuge rund um den **Kauf von Twitter durch Elon Musk** stellt sich auch die Frage, ob diese Plattformen nicht fast schon so etwas wie **public infrastructure** 
geworden sind, z.B. für den **Meinungsaustausch**, wo spezifisch Twitter eine wichtige Rolle spielt(e).

{% include 'edu_competences.md' %}

## Diskussions-Arena zum Thema Platform-Regulierung

Es ist immer spannend, wenn man ein **Thema kontrovers** diskutiert, so wie das die **Arena im SRF** seit Jahrzehten tut. Heute machen wir unsere 
eigene **Arena** und gruppieren uns in Teams, die dann **verschiedene Standpunkte** einnehmen sollen. 

!!! info "Einspieler während der Sendung" 
    Um eure **Argumente zu stützen**, kann die **Kurs-Leitung** ensprechende **Websites** auf dem **Beamer** für die Klasse anzeigen, 
    wie das bei **Einspielungen bei der Arena im Fernsehen** auch gemacht wird.

**Mögliche Standpunkte und Positionen**:

- So viel Eingriff wie nötig, so wenig wie möglich
- Staat muss jetzt handeln und etwas gegen Nebeneffekte von Social Media tun
- Platformen müssen selbst verhindern, dass Fakenews und Hatespeech verbreitet werden
- Kein Verständnis für staatliche Eingriffe zur Steuerung der politischen Meinung, keine neuen Regulierung

## Vorbereitung auf die Diskussion

Wir wollen nicht unvorbereitet (oder unbewaffnet mit Argumenten :fire:) in die Diskussion einsteigen. 
Da es sich um sehr breites Thema handelt, stehen unten allerlei Fragen, die man sich stellen kann.

1. **Wählt eine politische Position**, die ihr gerne vertreten würdet. Sucht eure Gruppe zusammen.
2. **Recherchiert** anhand der Links weiter unten zum Thema **in der Gruppe**.
3. Macht **handschriftliche Notizen** mit den **wichtigsten Argumenten**, allenfalls mit Hinweis auf Quellen
 
### Anregungen

- Wie können wir uns **vor Abstimmungen vor Fake News schützen**?
- Wie kann man **sich wehren**, wenn jemand auf einer Plattform meinen Ruf schädigt oder mich übel beleidigt?
- Wie können die **Gesetze**, die in real bestehen, auch auf den Platformen **durchgesetzt werden**?
- Sind ***Falschnachrichten*** ein **Problem für die Demokratie**, in der Schweiz (und generell)?
- Welche Beispiele in der Welt gibt es, wo **Social Media Abstimmungen wahrscheinlich beeinflusst haben**?
- Sollte man bei der Regulierung von Platformen auf Europa schauen?
- Müssen wir **eigene Regeln** machen oder werden wir einfach die von der EU übernehmen können, weil die Schweiz so klein ist?
- Welche **Regelungen** hat die **EU** für die Zukunft angedacht (**DSA**, Safe Harbor 2, ...)?
- Wie kann man **FakeNews erkennen**?
- **FakeNews**: Seit wann verwenden wir diesen Begriff? **Woher kommt der**?
- Ist unsere **Medienkompetenz** in der Bevölkerung **ausreichend** und welche **Unterschiede** gibt es in den **Alterklassen**?
- **Mainstream-Medien**: Häufig gehört, ist dies nicht einfach eine Diffamierung? Sind die **Medien** noch **divers genug**?
- Kann **Demokratie** noch funktionieren, wenn es keinen **common ground** mehr gibt und alle etwas anderes glauben?
- Was sind **Bubbles und Echokammern**, und leben wir nicht **alle in verschiedenen Bubbles**?
- **Wahr und unwahr**: Wo ziehen wir die Grenze? Wer entscheidet? Gibt es allenfalls Abstufungen von *Wahrheit*?
- Gab es nicht schon immer Falschnachrichten und Propaganda? Wie verbreiten sich Informationen?
- Was ist die **Rolle der Angst** bei der Beeinflussung von Menschen?
- Können wir den **Platformen Regeln geben**, damit diese **demokratischer** werden? Wenn ja, **wo ansetzen**?
- Was sind die **Chancen von Social Media** für die Demokratie?
- Was sind die **Gefahren von Social Media** für die Demokratie?
- Was ist Zensur?
- Können Manipulationen durch Bots erkannt werden?
- **Zensiert ein Algorithmus auch**, indem er über die Selektion der Inhalte entscheidet?
- Wo liegen die **Probleme von automatischen Entscheidungsystemen** bei der Bekämpfung von kriminellen Aktivitäten, Hass, oder Fake News?
- Wollen wir, dass unser IPhone unsere Bilder auf dem Telefon in Echtzeit auf CSAM scannt?
- Wie hoch müssen **Bussen** sein, damit diese nicht kleiner sind, als die **Gewinne**, die man mit einem Verstoss macht?
- Wie wird **maschinelles Lernen** unsere KMU und die Wirschaft beeinflussen? Sind **Super-Konzerne wie Meta eine Gefahr** für die Wirtschaft?
- Verstehen unsere **eigenen Politiker**, wie die **werbebasierte Digitalwirtschaft** funktioniert mit all unseren Daten?
- Brauchen wir nicht dringend **junge Leute**, die in die **Politik gehen** und digitale Welt besser verstehen?

## Links

- [Medienqualitäts-Rating der Universität Zürich]({{ urls.mqr_schweiz }})
- [DSA]({{ urls.dsa_eu }})
- [WSJ Facebook Files]({{ urls.wjs_facebook_files }})
- [https://dnip.ch/](https://dnip.ch/)
- [https://inside-it.ch](https://inside-it.ch)
- [SrfDigital Podcast](https://www.srf.ch/audio/digital-podcast)
- [Paul-Olivier Dehaye - eigene Daten nutzbar machen](https://digipower.academy/)
- [https://media.ccc.de](https://media.ccc.de)
- [Ratgeber Digitale Selbstverteidigung]({{ urls.ratgeber_digitale_selbstv }})
- [IT Blog Mike Kuketz](https://www.kuketz-blog.de/empfehlungsecke)

{% endblock %}

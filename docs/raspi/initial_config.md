---
title: Pi aufsetzen
description: Anleitung für die ersten Schritte bei der Raspi-Installation
---

# Raspberry Pi OS (former Raspian)

Je nach Anwendung, kann man auch das Lite image verwenden, insbesondere wenn man das Raspi in Funktion eine Servers betreibt (VPN, Nextcloud, PiHole etc).

[-> Download Raspberry Pi OS](https://www.raspberrypi.org/software/operating-systems/#raspberry-pi-os-32-bit)

# SD-Karte vorbereiten

Mit der Kommandozeile erst die SD-Karte identifizieren.

    lsblk

Mit `dd` kann man das Image auf den Datenträger schreiben ohne dass man dafür ein Tool braucht:

```bash
dd if=<imagename.img> of=/dev/mmcblk0 status=progress bs=16M
```

# Raspberry Pi OS Grundeinstellungen

Nach dem ersten Start mit dem Light Image muss man erstmals das Passwort eingeben. Das **Standard-Passwort** für den Nutzer `pi` ist **`raspberry`** (Achtung wenn Tastatur EN eingestellt ist, ist y das z).


1. Passwort ändern: `sudo passwd pi`.
2. Richtiges Tastatur-Layout und Sprache, SSH Service weiteres: `sudo raspi-config`

## Betrieb als Server

SSH ist per standard deaktiviert. Aktivieren geht über `sudo raspi-config`. Dort kann man auch einstellen, ob das Raspi mit Desktop gestartet werden soll. Für den Betrieb als Server eignet sich das **Raspberry Pi OS Light** am Besten.


Auf **dem Rechner für den Zugriff** müssen ssh keys vorhanden sein. Das Verzeichnis für die Keys ist `~/.ssh`.


Ein ssh Schlüssel kann kopiert werden auf das pi mit

    cd ~/.ssh
    ssh-keygen -t ecdsa
    ssh-copy-id pi@192.168.0.xx


Testen, ob Login ohne Passwort möglich ist

    ssh -v pi@192.168.0.xx

Am Ende sollte **unbedingt** die unsichere Passwortauthentifizierung deaktviert werden. Die folgenden Schritte auf dem Raspi ausführen:

    sudo nano /etc/ssh/ssh_config

Suche nach der Zeile `# PasswordAuthentication no` und ersetze sie durch `PasswordAuthentication no` indem du das Kommentarzeichen entfernst. Um die Änderungen zu aktivieren:

    sudo systemctl restart sshd.service

{% include 'glossary.md' %}
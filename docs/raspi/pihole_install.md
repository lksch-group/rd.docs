# Installation pihole

Wichtig ist, auf dem Router einzustellen, dass es dem pihole Raspi immer die **gleiche IP** zuweist. Ansonsten kann es vorkommen, dass 
das pihole nach dem Neustart nicht mehr funktioniert.

## Zusammenfassung aller Schritte

Initial muss das Rapsi erst an einen Bildschirm angeschlossen werden, später kann man 
über einen anderen Computer darauf zugreifen, damit kein Bildschirm benötigt wird.

- Raspi, Ethernet-Kabel, SD-Karte, Bildschirm, Tastatur bereitlegen
- Password für das Login auf dem Router bereithalten
- Image Raspberry Pi OS Lite auf die SD-Karte schreiben (Empfohlen: [Raspberry Pi Imager]({{ urls.raspi_software }}))
- Raspi starten und `raspi-config` Menu's durchgehen
- SSH absichern
- Auf dem Router einstellen, dass er dem Raspi immer die gleiche IP-Adresse zuweist
- PiHole installieren
- Router DNS auf die Adresse des pihole abändern
- Auf Admin-Interface einloggen und geniessen!

**Installation**

Die Installation kann man mit [einem Befehl auf der Kommandozeile starten](https://github.com/pi-hole/pi-hole/#one-step-automated-install):

    curl -sSL https://install.pi-hole.net | bash

Eine bebilderte Anleitung findet man auf [gnulinux.ch](https://gnulinux.ch/pihole-installation-und-betrieb). Am Ende wird das *Passwort* für das *Admin-Dashboard* angezeigt. Starte deinen **Browser** und gib ein:
    
    http://pi.hole/admin

    # sofern das nicht funktioniert
    http://<statische_ip_addresse_des_pi>/admin

Sofern das pihole jetzt aktiv ist, kennt es diese Adresse und leitet dich auf die vorher statisch definierte IP weiter.

pihole bietet ein Kommandozeilen-Werkzeug, die Optionen kann man abrufen via

    pihole --help

## Live-Logs

Will man live beobachten, was verbundene Geräte machen, schaut man sich die Logs direkt an:

    sudo pihole -t

## Rekonfiguration nach Installtion

Jede Einstellung kann auch nach der Erstinstallation nochmals verändert werden:

    sudo pihole -r

## Erklärungen zur Funktionsweise

Der Name *pihole* ist nicht zufällig gewählt. Er ist abgeleitet von **DNS sinkhole**, was man auf Deutsch liebevoll mit *DNS-Doline* übersetzen könnte =). Das sagt aber nicht viel aus, besser kann man es sich so merken:

Die DNS-Funktion ist durch Server im Internet gewährleistet, die wie Telefonbücher sind. Wenn du die Seite [gnulinux.ch](https://gnulinux.ch/) besuchst, dann möchtest du dem Betreiber der Website vereinfacht ein Paket senden, er soll dir doch bitte den Inhalt der Seite zurückschicken. Um die Adresse zu erfahren, wo dieses Paket hingeschickt werden soll, muss jetzt also das Telefonbuch (ein DNS-Server) gefragt werden. Dieser übersetzt dann den Namen in eine IP-Adresse, soz. die Hausnummer der Serves, wo die Webseite sich befindet.

Es gibt 3 verschiedene Nutzungs-Szenarien, auf alle möchte ich kurz eingehen.

### **Option 1**: pihole in Router als DNS-Server eingestellt

- Alle Geräte im (W)LAN profitieren automatisch vom pihole (Smartphones!)
- Das pihole sieht, dass alle Anfragen vom Router kommen weil alle Geräte den Router nach DNS Information fragen

### **Option 2**: pihole DNS auf allen Geräten im LAN separat einstellen

- Sofern es möglich ist, kann man für das **jeweilige WLAN-Netz** einen eigenen DNS-Server konfigurieren und dort die **IP des pihole eintragen**.
- Dann kann das pihole die **Anfragen nach Geräten aufschlüsseln**, weil die Anfrage von der IP des Gerätes direkt stammt.
- Idealerweise vergibt dein Router für deine Geräten auch immer die gleiche IP, wenn du wissen willst, von welchen Geräten welche DNS-Anfrage kommen.
- Evtl. kann man nur für die Netzwerkkarte einen DNS-Server definieren. Das kann man für stationäre Desktop's machen, bei mobilen Geräte verlangsamt sich dann aber alle Interaktion mit dem Internet zu sehr, da immer erst eine lokale IP angefragt wird, die in einem fremden Netz nicht existiert, und erst dann wird ein öffentlicher Resolver (DNS-Server) wie z.B. der von Quad9 (9.9.9.9) benutzt wird (fallback, weil meist hinterlegt man 2 DNS-Server).

<figure>
  <img src="../../img/iface_custom_dns.png" />
  <figcaption>Einstellung Netwerkkarte DNS bei einem Desktop-Computer, Gnome Desktop</figcaption>
</figure>

!!! tip

    **Beides kombinieren!** Beim Smartphones lässt sich oft kein DNS einstellen. Dann greift die Weiterleitung aus **Option 1**. Für alle anderen Geräte, deren Anfragen man genauer wissen will, stellt man **Option 2** ein.

### **(Option 3)**: Das pihole wird als DHCP konfiguriert

Es muss zwingend das **DHCP des Routers deaktiviert** werden, damit nicht zwei Geräte glauben, sie dürften deinen Geräten nach Lust und Laune eigene, lokale IP-Addressen zuweisen, was zu Chaos führt. Der Vorteil dieser Konfiguration wäre, dass das pihole nun immer genau weiss, wer die Anfragen stellt und man dadurch die genauste Aufzeichnung des Verhaltens aller Geräte bekommt.

!!! warning

    Fällt das Raspi aus, funktioniert dein (W)LAN nicht mehr. Es auch wahrscheinlich, dass dein Router diese Aufgabe viel besser erledigt, wenn du nicht noch zusätzliche Einstellungen am pihole machst.

## Fazit

Für die Einfachheit der Installation bietet das pihole sehr viel. Und kaputtmachen kann man dabei auch nichts. Wenn das pihole einmal ausfällt, dann macht sich das daran bemerkbar, dass Webseiten verlangsamt laden. Da - egal ob auf dem Router oder bei einzelnen Geräten - immer 2 oder mehr DNS-Server als IP-Addressen hinterlegt sind, wird auch bei einem Ausfall nichts kaputtgehen. Um das Problem zu beheben, entfernt man eifach die IP des pihole aus dem Router oder dem Gerät.

## Weiteres

Raspi haben einen gemeinsamen Block in der MAC-Adresse: `B8:27:EB`. So findet man alle Raspis im Netzwerk:

```bash
sudo nmap -sP 192.168.0.1/24 | awk '/^Nmap/{ip=$NF}/B8:27:EB/{print ip}'
```

{% include 'glossary.md' %}
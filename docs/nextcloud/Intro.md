---
title: Hardware für Nextcloud
description: Entscheidungshilfen für die Wahl der Hardware für die Nextcloud
install_options:
  - name: Laptop
    power: 10 - 20
    pros:
      - Durch Batterie gegen Stromausfall abgesichert
      - Gutes Preis / Leistungsverhältnis
    CHF: 100 - 200
    icon: ':fontawesome-solid-laptop:'
  - name: Raspi
    power: '2.5'
    pros:
      - klein und sparsam
    cons:
      - langsame MicroSD
      - nur für wenige Nutzer gleichzeitig geeignet
      - meist externe Festplatte nötig
    CHF: 80 - 160
    icon: ':fontawesome-brands-raspberry-pi:'
    logo_url: '../logos/Raspberry_Pi_Logo.svg'
    logo_width: 70

---
# Nextcloud

## Übersicht Installationen

### Gerätevergleich

{% set items =  install_options %}
{% include 'tabbed_pros_cons.md' %}

Am Ende stellt sich der Laptop in Punkto Grösse, Preis-Leistung und Lärm und Ausfallsicherheit als eine gute Lösung dar. Man kriegt sehr günstig einen alten Laptop, dem man noch eine SSD verpassen kann und preislich gleich liegt wie mit einem Raspi mit externer Festplatte.

## Voraussetzungen

### Routerauswahl

Der Router ist auch essentiell in der Installation. Der Router sollte folgendes am besten können: **DynDns Account konfigurieren**, **IPV4 unterstützen**, **PortForwarding unterstützem**.

Zur Überprüfung würde ich die Einstellungen einmal suchen gehen. Der Router ist meist unter `192.168.0.1` erreichbar.

| Router   | IPV4                  | DynDNS | Port Forwarding   | als Modem | Fazit                           |
| -------- | --------------------- | ------ | ----------------- | --------- | ------------------------------- |
| Cablecom | kann deaktiviert sein | ?      | sofern IPV4 aktiv | ok        | ok                              |
| Swisscom | ok                    | ok     | ok                | ok        | ok                              |
| Wingo    | x                     | x      | x                 | ???       | mit Zusatzrouter im Modem Modus |
| Fritzbox | ok                    | ok     | ok                | ok        | ok                              |

## Betrieb Cloud zu Hause

- DynDNS Account (z.B. https://desec.io/) oder ein Provider, der auf dem Router schon angeboten wird z.B. noip.com.
- oder feste IP beim Provider kaufen (kostet Geld)
- Anfragen an den Router auf Port **80** und **443** müssen an die Cloud weitergeleitet werden (PortForwarding). Port **22** für SSH Verbindungen
- Weiteren Speicher vorhalten, um die Daten zu backupen

## Nextcloud ins Internet kriegen

- Ort der Installation sollte am besten der Ort sein, wo das Gerät dann auch langfristig bleibt.
- Zum Zeitpunkt der Installation muss die Internet-Adresse auf den Ort zeigen, an dem man gerade ist, da sonst die SSL-Zertifikate nicht erzeugt werden können.

### Vorbereitungen für eine Lerngruppe

Sofern die Installation in einer Gruppe von Lernenenden erfolgt, wäre es für jede Person von Vorteil, wenn sie sich ihr Gerät schon ausgesucht hat und dieses von zu Hause mittels SSH Verbindung erreichbar macht, sodass man darauf von Remote aus arbeiten kann. Das heisst: Router sowie DynDNS müssen dann schon korrekt konfiguriert sein.

Dieser Schritt wäre idealweise über Videoconferencing zu erledigen.

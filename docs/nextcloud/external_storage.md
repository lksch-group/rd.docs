## Erweiterung Nextcloud Speicher (ExternalStorage)

Der External StorageSupport erlaubt es, weiteren Speicher anzubinden. Ich würde davon wenn möglich abraten und gleich eine grosse Festplatte nehmen.

### Via CIFS auf NAS

- Problem waren teilweise die Filepermissions, die auf der NAS und der Nextcloud nicht identisch waren. Es kommt zu einem Durcheinander.

- Perfomance ist auch nicht sonderlich gut über dieses Protokoll
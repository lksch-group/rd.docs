# Installation Nextcloud

Anbei ein paar hilfreiche Quellen für die Installation von Nextcloud auf einem Debian basierten Linux.

## Quellen

- [Carsten Rieger](https://www.c-rieger.de/) hat sehr gute Scripts und Anleitungen, um sich die Nextcloud selber zu installieren.
- [Nextcloud Admin Manual](https://docs.nextcloud.com/server/stable/admin_manual/)

## Empfehlungen für Hosting

- Nextcloud Instanz bei [LinuxFabrik](https://blog.linuxfabrik.ch/2021/06/k-tipp-nextcloud-linuxfabrik-und-mailbox-sind-testsieger/): [https://ws.linuxfabrik.io/](https://ws.linuxfabrik.io/)
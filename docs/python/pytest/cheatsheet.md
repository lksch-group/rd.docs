---
title: pytest cheatsheet
tags:
  - pytest
  - testing
---

Run a specific test file

```bash
pytest tests/tests_api.py
```

Run a specific test function:

```bash
pytest tests/test_utils.py::test_sort_modules
```

Run the last failed tests only

```bash
pytest --lf
```

Step the tests when, x amount of tests failed:

```bash
pytest --maxfail 2
```

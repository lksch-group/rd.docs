---
title: Config management
tags:
  - Python
  - 12factor
  - API
  - CLI
---

{% extends 'page_base.md' %}
{% block content %}

## Config via environment values

The 12factor methodology outlines a set of best practices for building modern, scalable applications ready to be containerized, 
and one of its **key principles** is to **store configuration in the environment.** 
Configuration is read at runtime and delivers any data that an application needs to run properly, such as database credentials, API keys, and other settings. 
The application behaviour can change upon restart, which provides the necessary flexibility to test and run these applications on different operating systems and as **containers**. 
A lot of **command line applications** also feature configuration trough environment variables for the default of arguments/options.

## Types of configuration

**Different types of configuration** should be **treated differently**, and they come probably from **different sources**. Consider these two environment 
variables that might be used both in a frontend and/or backend context:

- BACKEND_URI:
    - is **public** 
    - used at **build time** by a web app, and at **run time** by the API (e.g. *CORS*).

- DATABASE_URI:
    - **semi-secret**, should not be exposed in a git repo necessarily
    - needed at **run time** in backend services

Any other sensible secrets should be retrieved from a secret store that is specifically made for storing passwords. 
Some people use CI secrets when deploying directly from CI, or retrieve them through other secret store solutions. 
There is a limit of the size of environment variables in different systems and contexts, but a **value** could well be 
given as **json string**.

## File types

=== "json"

    - officially specified and does not change anymore
    - limited number of types
    - can be validated with jsonschema

=== "toml"

    - is valid json and can be validated alike
    - reminds a little of ini file sections
    - one of the preferred new config file formats
    - ambiguity in how the file is written, but less than in yaml
    - Today many agree that the toml format is a good option, being human-friendly and reliable like json

===  "yaml"

    - yaml has oddities because you can add custom parsers
    - different versions of yaml is not what you want for a config file format
    - yaml is not a python builtin library (json and toml are)
    - patching yaml in python is only possible keeping comments using `ruaml.yaml` library where `PyYaml` can't

===  "env"

    - actually not specified format
    - cind of an agreement that it should be valid bash
    - still widely used


=== "ini"

    - used by ansible for inventories alongside yaml
    - builtin python package to read the format and was also widely used
    - will be seen less in the future

## Comparison of libraries

Let's have a simple env file containing a **flat list** of key-value pairs:

{{ macros.file_include('examples/python/config_mgmt/env') }}

### `pydantic.BaseSettings`

The `pydantic.BaseSettings` uses the `dotenv` library in the background. This library is only capable of reading 
env files. Below examples of how to use different env files or a **single env** file (as above) and still achieve 
nicely validated configs which can be nested as well.

{{ macros.pytest_include('config_mgmt/doc_test_pydantic_with_file.py') }}

**Fazit**

Data validation, being it from environment variables or other, benefit from the following:

- various helper types for validation, such as `SecretStr`, `DirectoryPath` etc.
- **fine-grained validation for nested configs** and immediate feedback what failed
- composable settings
    - by class inheritance
    - by nesting settings
- json serializable plus **jsonschema**, which can be used by various other tooling like **editor autocomplete features**


### `confz`

This time, we provide the same contents as in the previous `env` file as a yaml file:

{{ macros.file_include('examples/python/config_mgmt/confz_env.yml') }}

{{ macros.pytest_include('config_mgmt/doc_test_confz_with_file.py') }}

## `django.environ`



{% endblock %}

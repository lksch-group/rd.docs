---
title: Zen of Python
tags:
  - Python
  - PEP
---

# Zen of Python

The Zen of Python are principles to follow for the programming language. It is included 
in [PEP20](https://peps.python.org/pep-0020/) from 2004.

- **Beautiful** is better than ugly.
- **Explicit** is better than implicit.
- **Simple** is better than complex.
- Complex is better than complicated.
- **Flat** is better than nested.
- **Sparse** is better than dense.
- **Readability** counts.
- Special cases aren't special enough to break the rules.
- Although practicality beats purity.
- **Errors should never pass silently**.
- Unless explicitly silenced.
- In the face of ambiguity, refuse the temptation to guess.
- **There should be one-- and preferably only one --obvious way to do it.**
- Although that way may not be obvious at first unless you're Dutch.
- Now is better than never.
- Although never is often better than *right* now.
- **If the implementation is hard to explain, it's a bad idea.**
- If the implementation is easy to explain, it may be a good idea.
- **Namespaces** are one honking great idea -- let's do more of those!

## Codestyle

Python has conventions of how to indent, how to name variables etc. Those are summarized 
in [PEP8]({{ urls.pep8_home }}). It is **best to learn these conventions while programming**, 
since your IDE has support for these formatting patterns and will probabely **underline** variables 
that do not confirm conventions. It is also best practice to use a **linter**, e.g. `flake8` to 
ensure code formatting follows the standards.

{% include 'glossary.md' %}
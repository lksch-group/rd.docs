---
title: Jinja2 template engine
tags:
  - ansible
  - mkdocs
---

The [Jinja2 template engine]({{ urls.jinja2_home }}) is the most widely used template engine for python out there. 
It is done by the same company as the cli toolkit [click]({{ urls.click_home }}). Other project that use Jinja2 are

- [ansible]({{ urls.ansible_github }})
- Almost all python web frameworks supporting html templating

## Reference

- [Jinja2 Homepage]({{ urls.jinja2_home }})
---
title: Python Codestyle - Avoid these patterns
---

## How python imports modules

**:octicons-alert-16: Initializing expensive objects on module level**

If `def calculate` is needed in another file and imported there, load data is run when 
the function is imported. **Best avoided by putting all logic into functions**.

```python
--8<-- "examples/expensive_module_init.py"
```

**:octicons-alert-16: Import ***

This is a pattern should be avoided because:

- It imports everything when most of the time just certain objects are needed
- It is not transparent what objects were imported

```python
from datetime import *
```
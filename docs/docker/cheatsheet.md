---
title: Docker Cheatsheet
image_name: 'nginx:latest'
service_name: 'nginx_service'
tags:
  - docker
  - cheatsheet
---
## docker cheatsheet

Cleanup unused images:

    docker image prune -f

Remove stopped containers:

    docker container prune -f

View logs of a running container:

    docker logs -f {{ image_name }}
    docker-compose logs -f {{ service_name }}
 
View logs of all running services of a `docker-compose.yml` file:

    docker-compose logs -f

Enter an already running container using bash in interactive mode:

    docker exec -it {{ image_name }} /bin/bash

Inspect an image by running a container:

    docker run --rm -it {{ image_name }} /bin/bash

Execute a command inside a container, removing the container afterwards

    docker run --rm {{ image_name }} ls

Inspect a containers environment variables:

    docker run --rm {{ image_name }} env

List docker networks and images:

    docker network list
    docker image list

List running containers:
    
    docker ps

List all containers, also stopped:

    docker ps -a

## docker-compose cheatsheet

Show validated `docker-compose.yml` config:

    docker-compose -f docker-compose.yml config

!!! tip

    This is especially handy if you use these files in your program tooling e.g. if you automating things
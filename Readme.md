# Real | Digital | Docs

## Get started

Install dependencies:

    python3 -m venv venv
    source ven/bin/activate
    pip install -r requirements.txt

Run live server:

    mkdocs serve

## Privacy notice

- No additional Google Fonts are loaded
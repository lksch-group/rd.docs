from functools import lru_cache
from pathlib import Path
from typing import Callable, Tuple, Optional, Iterable

import click
import re
from rich import print

YamlSplitT = Tuple[Optional[str], str]
ProcType = Callable[[Optional[str], str, Path], YamlSplitT]


def multi_glob(path, patterns: Iterable[str]):
    """Returns str objects, not Path objects"""
    p = Path(path)
    for patt in patterns:
        yield from p.glob(patt)


@lru_cache(typed=True)
def load_yaml_config(filename: str = "mkdocs.yml"):
    """Uses the mkdocs yaml loader supporting all the necessary features"""
    from mkdocs.utils import yaml_load

    with open(filename, "r") as f:
        data = yaml_load(f)
    return data


def get_docs_dir(config: Optional[dict] = None) -> Path:
    if config and "docs_dir" in config:
        return Path(config["docs_dir"])

    p = Path("docs")
    assert p.is_dir()
    return p


def split_yaml_file(raw_yaml: str) -> YamlSplitT:
    header = []
    lines = raw_yaml.splitlines()
    for ix, line in enumerate(lines):
        # the first line
        if not ix:
            if line == "---":
                header.append(line)
                continue
            else:
                return None, raw_yaml
        # we deal with a header
        if line == "---":
            header.append(line)
            return "\n".join(header), "\n".join(lines[ix + 1 :])
        else:
            header.append(line)
    assert False, "should not reach this point"


@click.group()
@click.option(
    "--config",
    type=click.Path(path_type=Path, file_okay=True),
    default="mkdocs.yml",
    show_default=True,
)
@click.pass_context
def cli(ctx, config):
    """A mkdocs helper cli tool with the aim of increasing the consistency of your project"""
    ctx.obj = load_yaml_config(config)


def process_file(
    file: Path,
    processors: Iterable[ProcType],
    dry_run: bool = False,
    check: bool = False,
) -> None:
    header, content = split_yaml_file(file.read_text())
    new_content = content
    new_header = header
    for proc in processors:
        new_header, new_content = proc(header, content, file)
    if new_content != content or new_header != header:
        if not dry_run:
            file.write_text("".join((new_header or "", new_content)))
        elif check:
            print("".join((new_header or "", new_content)))


@cli.command()
@click.option(
    "-e", "--ending", "endings", multiple=True, show_default=True, default=["md"]
)
@click.option("--dry-run", is_flag=True, default=False)
@click.option("--check", is_flag=True, default=False)
@click.pass_obj
def fix_urls(config, endings, dry_run, check):
    """Searches for urls available mkdocs extra config in markdown files."""
    mkdocs_config = load_yaml_config()
    docs_dir = get_docs_dir(config)

    def config_urls(yaml_conf: dict) -> dict:
        out = {}
        patt = re.compile(r"^https?:")

        def scan(sub: dict, parent_key: str = ""):
            for key, val in sub.items():
                if isinstance(val, dict):
                    scan(val, key)
                elif isinstance(val, str) and patt.search(val):
                    if parent_key:
                        out[f"{parent_key}.{key}"] = val
                    else:
                        out[key] = val

        scan(yaml_conf["extra"])
        return out

    def _fix_urls(header: Optional[str], content: str, file: Path):
        """Looks for urls inside the content that are also provided by mkdocs extra variables. Replaces the url so that
        the urls are in one place"""
        for path, url in config_urls(mkdocs_config).items():
            if url in content:
                if check or dry_run:
                    print(f"Found URL in {file.parts[-1]}:\tReplace {url} with {path}")
                content = content.replace(url, f"{{{{ {path} }}}}")
        return header, content

    globs = (f"**/*.{ending}" for ending in endings)

    for file in multi_glob(docs_dir, globs):
        process_file(file, processors=(_fix_urls,), dry_run=dry_run, check=check)


def inject_base_page_extension(
    header: Optional[str], content: str, base_page_name: str
) -> str:
    exclude = {"{% include 'glossary.md' %}"}
    for entry in exclude:
        content.replace(entry, "")
    if not header:
        return "\n".join(
            (
                f"{{% extends '{base_page_name}' %}}",
                "{% block content %}",
                content,
                "{% endblock %}",
            )
        )

    return "\n".join(
        (
            f"{{% extends '{base_page_name}' %}}",
            "{% block content %}",
            content,
            "{% endblock %}",
        )
    )


@cli.command()
@click.option(
    "--base-page",
    help="The name of a markdown file available as macros.include_dir from the mkdocs-macros plugin",
)
@click.option("--dry-run", is_flag=True, default=False)
@click.option("--check", is_flag=True, default=False)
@click.pass_obj
def check_pages(config, base_page, dry_run, check):
    """
    Checks all *.md pages for
    - header with title present or not
    - If the page is extended with the base page if specified
    """
    docs_dir = get_docs_dir(config)

    def base_page_inject(header: Optional[str], content: str, file: Path):
        if not base_page:
            return header, content
        if f"extends '{base_page}'" not in content:
            if file.parts[-1] == base_page:
                return header, content
            if check or dry_run:
                click.secho(f"NO BASE_PAGE: {file}")
            if not dry_run:
                return header, inject_base_page_extension(header, content, base_page)
        return header, content

    def check_header(header: Optional[str], content: str, file: Path):
        if not header and dry_run or check:
            click.secho(f"NO HEADER: {file}")
        return header, content

    for file in docs_dir.glob("**/*.md"):
        process_file(
            file,
            processors=(check_header, base_page_inject),
            dry_run=dry_run,
            check=check,
        )


if __name__ == "__main__":
    cli()
